package bamboo

import (
	"net/http"
	"path"
	"compress/gzip"
	"strconv"
	//"fmt"
)

type Http struct{
	O http.ResponseWriter
	I *http.Request
	Dir string
	File string
	Ext string
	URI string
	BoValue map[string]string
	Age int
	ret bool
	sc int
}

func (wr *Http) GetCookie(name string) string {
	ck, _ := wr.I.Cookie(name)
	return ck.Value
}

func (wr *Http) SetCookie(name string, value string, maxAge int, domain string) {
	c := http.Cookie{
		Name: name,
		Value: value,
		MaxAge: maxAge,
	}

	if domain != ""{
		c.Domain = domain
	}

	wr.O.Header().Add("Set-Cookie", c.String())
}

func (wr *Http) wrt(mime string, data []byte, flietime string) {
	wr.ret = true

	if flietime == ""{
		len := len(data)
		if len != 0 {
			eTag := etagHat + strconv.Itoa(len) + "." + strconv.Itoa(Hash2(data))
			if wr.I.Header.Get("If-None-Match") == eTag {
				wr.O.WriteHeader(304)
				return
			}else{
				wr.O.Header().Set("Etag", eTag)
			}
		}
	}else{
		if wr.I.Header.Get("If-Modified-Since") == flietime {
			wr.O.WriteHeader(304)
			return
		}else{
			wr.O.Header().Set("Last-Modified", flietime)
		}
	}

	wr.O.Header().Set("Cache-Control", "max-age=" + strconv.Itoa(wr.Age))
	wr.O.Header().Set("Content-Type", mime)

	//wr.O.Header().Set("Access-Control-Allow-Origin", "*") //跨域AJAX

	if mime == "text/javascript; charset=utf-8" || mime == "text/html" || mime == "text/xml" || mime == "text/css; charset=utf-8" || mime == "image/bmp"{
		wr.O.Header().Set("Content-Encoding", "gzip")
		wr.O.WriteHeader(wr.sc)
		z := gzip.NewWriter(wr.O)
		z.Write(data)
		z.Close()
	}else{
		wr.O.WriteHeader(wr.sc)
		wr.O.Write(data)
	}
}

func (wr *Http) Print() {
	wr.RetBo(boPrint)
}

func (wr *Http) RetHtml(data string) {
	wr.wrt("text/html", []byte(data), "")
}

func (wr *Http) RetText(data string) {
	wr.wrt("text/plain", []byte(data), "")
}

func (wr *Http) RetDefault() {

	data, time, err := ReadFile(pubDir + wr.File)
	var mime string

	if err != nil{
		if wr.File[len(wr.File)-1:] == "/"{
			data, time, err = ReadFile(pubDir + wr.File + "index.html")
			if err == nil{
				mime = "text/html"
			}else{
				data, time, err = ReadFile(pubDir + wr.File + "index.htm")
				if err == nil{
					mime = "text/html"
				}else{
					data, time, err = ReadFile(pubDir + wr.File + "index.bo")
					if err == nil{
						mime = "text/html"
					}
				}
			}
			
		}else{
			data, time, err = ReadFile(pubDir + wr.File + "/index.html")
			if err == nil{
				mime = "text/html"
			}else{
				data, time, err = ReadFile(pubDir + wr.File + "/index.htm")
				if err == nil{
					mime = "text/html"
				}else{
					data, time, err = ReadFile(pubDir + wr.File + "/index.bo")
					if err == nil{
						mime = "text/html"
					}
				}
			}
		}
	}

	if err == nil{
		if mime == ""{
			if wr.Ext == ""{
				mime = "application/octet-stream"
			}else{
				mime = mimes[wr.Ext]
				if mime == "" {
					mime = "application/octet-stream"
				}
			}
		}
		wr.Age = 600
		wr.wrt(mime, data, TimeToHttpString(time))
	}else{
		wr.NotFound()
		wr.Print()
	}
}

func (wr *Http) RetFile(url string) {

	data, time, err := ReadFile(pubDir + url)

	var mime string

	if err == nil{
		if mime == ""{
			if wr.Ext == ""{
				mime = "application/octet-stream"
			}else{
				mime = mimes[wr.Ext]
				if mime == "" {
					mime = "application/octet-stream"
				}
			}
		}
		wr.Age = 600
		wr.wrt(mime, data, TimeToHttpString(time))
	}else{
		wr.NotFound()
		wr.Print()
	}
}

func (wr *Http) RetBo(data string) {
	wr.wrt("text/html", []byte(ParseBo("/", data, wr.makeHttpBoValue(), srvBoValue, wr.BoValue)), "")
}

func (wr *Http) RetBoFile(url string) {
	data, _, err := ReadFile(boDir + url)
	if err == nil {
		wr.wrt("text/html", []byte(ParseBo(path.Dir(url), string(data), wr.makeHttpBoValue(), srvBoValue, wr.BoValue)), "")
	}else{
		wr.NotFound()
	}
}

//301 Moved Permanently 页面永久性迁移
func (wr *Http) Move(url string) {
	wr.ret = true
	wr.O.Header().Set("Content-Type", "text/html")
	wr.O.Header().Set("Location", url)
	wr.O.WriteHeader(301)
}

//302 Moved Temporarily 页面临时性迁移
func (wr *Http) Jump(url string) {
	wr.ret = true
	wr.O.Header().Set("Content-Type", "text/html")
	wr.O.Header().Set("Location", url)
	wr.O.WriteHeader(302)
}

//404 Not Found 页面没有找到
func (wr *Http) NotFound() {
	wr.sc = 404
}

func (wr *Http) makeHttpBoValue() map[string]string {
	return map[string]string{
		"HTTP.URL": wr.I.Host + wr.I.RequestURI,
		"HTTP.CORN": wr.I.Host,
		"HTTP.REFERER": wr.I.Referer(),
		"HTTP.FILE": wr.File,
		"HTTP.GOP": wr.I.Method,
		"HTTP.UA": wr.I.UserAgent(),
		"HTTP.SMOKE": strconv.Itoa(wr.sc),
	}
}

func (wr *Http) SplitDashUri() []string {
	var ret []string
	syg := 0
	for i := 0; i < len(wr.URI); i++ {
		if wr.URI[i] == '-' {
			ret = append(ret, wr.URI[syg:i])
			syg = i + 1
		}
	}
	return ret
}

func (wr *Http) SplitShortUri(long int) (string, string) {
	if len(wr.URI) >= long {
		return wr.URI[:long], wr.URI[long:]
	}
	return "", ""
}