package bamboo

import (
	"bytes"
	"github.com/russross/blackfriday"
	"path"
)

func ParseBoFile(url string, value ...map[string]string) string {
	return ParseBo(path.Dir(url), ReadFileString(boDir + url), value...)
}

func ParseBo(dir string, data string, value ...map[string]string) string {

	ret := bytes.Buffer{}
	var tagi, doci int

	for i := 0; i < len(data) ; i++ {
		if data[i] == '@' && data[i + 1] == '{' {
			ret.WriteString(data[doci:i])
			i+=2
			tagi = i
			for data[i] != '}' {
				i++
			}
			ret.WriteString(tagSwitch(data[tagi:i], dir, value...))
			i++
			doci = i
		}
	}

	ret.WriteString(data[doci:])

	return ret.String()

}

func tagSwitch(tag string, dir string, value ...map[string]string) string {
	switch {
		case len(tag) < 1:
			return ""
		case tag[0] == '"':
			if len(tag) < 3 {
				return ""
			}else{
				return tag[1:len(tag) - 1]
			}
		case tag[0] == '/':
			return ParseBo(dir, ReadFileString(boDir + dir + tag), value...)
		case tag[0] == '?':
			tagif := tag[1:]

			var tiaoj, zhi, tru, fals string
			var z, i int
			var e bool
			for i = 0; i < len(tagif) ; i++ {
				switch tagif[i] {
					case '=':
						tiaoj = tagif[:i]
						z = i + 1
					case ':':
						zhi = tagif[z:i]
						z = i + 1
					case '!':
						e = true
						tru = tagif[z:i]
						z = i + 1
				}
			}

			if e {
				fals = tagif[z:i]
			}else{
				tru = tagif[z:i]
			}

			if tagSwitch(tiaoj, dir, value...) == tagSwitch(zhi, dir, value...) {
				if tru != "" { //这里如果放上去一起if的话，如果tru==""，也会执行fals
					return tagSwitch(tru, dir, value...)
				}
			}else if fals != "" {
				return tagSwitch(fals, dir, value...)
			}
			
		default:
			for _ , d := range value {
				if d[tag] != "" {
					return d[tag]
				}
			}
	}
	return ""
}

var extensions int = 0
var renderer blackfriday.Renderer

func initMdParse() {
	// 设置 HTML 渲染
	htmlFlags := 0
	//htmlFlags |= blackfriday.HTML_SKIP_HTML // 跳过预处理HTML块
	//htmlFlags |= blackfriday.HTML_SKIP_STYLE // 跳过嵌入的 <style> 元素
	//htmlFlags |= blackfriday.HTML_SKIP_IMAGES // 跳过嵌入的图像
	//htmlFlags |= blackfriday.HTML_SKIP_LINKS // 跳过所有超链接
	//htmlFlags |= blackfriday.HTML_SANITIZE_OUTPUT // 剔除一切不安全的输出
	//htmlFlags |= blackfriday.HTML_SAFELINK // 超链接只能通往受信任的协议
	//htmlFlags |= blackfriday.HTML_NOFOLLOW_LINKS // 所有超链接阻止搜索引擎的抓取 
	//htmlFlags |= blackfriday.HTML_TOC // 生成一个表的内容
	//htmlFlags |= blackfriday.HTML_OMIT_CONTENTS // 跳到的主要内容（一个独立的表的内容）
	//htmlFlags |= blackfriday.HTML_COMPLETE_PAGE // 生成一个完整的HTML页
	//htmlFlags |= blackfriday.HTML_GITHUB_BLOCKCODE // 使用 github 上的代码风格
	//htmlFlags |= blackfriday.HTML_USE_XHTML // 生成输出XHTML，而不是HTML
	htmlFlags |= blackfriday.HTML_USE_SMARTYPANTS // 启用智能标点替换
	htmlFlags |= blackfriday.HTML_SMARTYPANTS_FRACTIONS // 启用智能分号 (with HTML_USE_SMARTYPANTS)
	htmlFlags |= blackfriday.HTML_SMARTYPANTS_LATEX_DASHES // 启用 LaTeX-style 破折号 (with HTML_USE_SMARTYPANTS)
	//htmlFlags |= blackfriday.HTML_FOOTNOTE_RETURN_LINKS // 产生在一个脚注的末尾链接返回到源

	renderer = blackfriday.HtmlRenderer(htmlFlags, "", "")

	// 设置 Markdown 解析扩展
	extensions |= blackfriday.EXTENSION_NO_INTRA_EMPHASIS // 里面的话，忽略重点标记
	extensions |= blackfriday.EXTENSION_TABLES // 渲染表格
	extensions |= blackfriday.EXTENSION_FENCED_CODE // 渲染代码块围栏
	extensions |= blackfriday.EXTENSION_AUTOLINK // 自动用URL生成没有明确标记的超链接
	extensions |= blackfriday.EXTENSION_STRIKETHROUGH // 删除线文本使用~~测试~~
	extensions |= blackfriday.EXTENSION_LAX_HTML_BLOCKS // 放松HTML块解析规则
	//extensions |= blackfriday.EXTENSION_SPACE_HEADERS // 严格前缀头规则
	extensions |= blackfriday.EXTENSION_HARD_LINE_BREAK // 强制转换换行符
	//extensions |= blackfriday.EXTENSION_TAB_SIZE_EIGHT //扩大TAB至八个空格，而不是四个
	//extensions |= blackfriday.EXTENSION_FOOTNOTES // Pandoc-style脚注
	//extensions |= blackfriday.EXTENSION_NO_EMPTY_LINE_BEFORE_BLOCK // 插入块元素（code, quote, ol, ul）前无需空行
	//extensions |= blackfriday.EXTENSION_HEADER_IDS // 指定元素的ID with {#id}
	//extensions |= blackfriday.EXTENSION_TITLEBLOCK // Titleblock ala pandoc
}

// 渲染 Markdown.Blackfriday
func ParseMd(data string) string {
	return string(blackfriday.Markdown([]byte(data), renderer, extensions))
}