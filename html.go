package bamboo

func QuoteJS(url string) string {
	return "<script type=\"text/javascript\" src=\"" + url + "\"></script>"
}

func QuoteCSS(url string) string {
	return "<link href=\"" + url + "\" rel=\"stylesheet\" />"
}