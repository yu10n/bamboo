package bamboo

const (
	//boPrintFmt = ".bo"
	boPrint = `
		<!DOCTYPE html>
		<html>
			<head>
				<title>找不到内容！</title>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<style type="text/css">
					body {
						background-color: #E5E5E5;
						font-family: "微软雅黑", "Helvetica Neue", Helvetica, Arial, sans-serif;
					}

					h1, a, a:hover{
						color: #fff;
					}

					.st {
						margin: 50px auto;
						width: 240px;
						height: 420px;
						background-color: rgb(83, 169, 63);
						box-shadow: 0px 5px 50px rgba(0, 0, 0, 0.8) ;
						padding: 50px;
					}
					.byz {
						display: inline-block;
						width: 90px;
						height: 90px;
						background-color: #fff;
					}

					.hyz {
						margin-top:20px;
						width: 40px;
						height: 40px;
						background-color: #000;
					}

					h1{
						font-size: 90px;
						font-weight: bold;
						margin: 150px 0 20px 0;
						text-rendering: optimizelegibility;
					}

					#text{
						text-align: center;
					}
				</style>
			</head>
  
			<body>
				<div class="st">
					<div class="byz">
						<div class="hyz"></div>
					</div>
					<div class="byz" style="float:right;">
						<div class="hyz"></div>
					</div>
					<div id="text">
						<h1>@{HTTP.SMOKE}</h1>
						<p><a href="@{BAMBOO.LINK}" target="_blank">@{BAMBOO.V}</p>
					</div>
				</div>
			</body>
		</html>
 `
)