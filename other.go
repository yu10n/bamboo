package bamboo

import (
	"os"
	"encoding/json"
	"path/filepath"
	"time"
	"bytes"
	"io"
	"strconv"
)

// 读文件到 String
func ReadFileString(url string) string {
	data, _, _ := ReadFile(url)
	return string(data)
}

func ReadFile(filename string) ([]byte, time.Time, error) {
    f, err := os.Open(rootDir + filename)
    if err != nil {
    	var niltime time.Time
        return nil, niltime, err
    }

    defer f.Close()
    // It's a good but not certain bet that FileInfo will tell us exactly how much to
    // read, so let's try it but be prepared for the answer to be wrong.
    var n int64
	fi, err := f.Stat()
    if err == nil {
        // Don't preallocate a huge buffer, just in case.
        if size := fi.Size(); size < 1e9 {
            n = size
        }
    }
    // As initial capacity for readAll, use n + a little extra in case Size is zero,
    // and to avoid another allocation after Read has filled the buffer.  The readAll
    // call will read into its allocated internal buffer cheaply.  If the size was
    // wrong, we'll either waste some space off the end or reallocate as needed, but
    // in the overwhelmingly common case we'll get it just right.
    d, e := readAll(f, n+bytes.MinRead)
    return d, fi.ModTime(), e
}

func readAll(r io.Reader, capacity int64) (b []byte, err error) {
	buf := bytes.NewBuffer(make([]byte, 0, capacity))
	// If the buffer overflows, we will get bytes.ErrTooLarge.
	// Return that as an error. Any other panic remains.
	defer func() {
		e := recover()
		if e == nil {
			return
		}
		if panicErr, ok := e.(error); ok && panicErr == bytes.ErrTooLarge {
			err = panicErr
		} else {
			panic(e)
		}
	}()
	_, err = buf.ReadFrom(r)
	return buf.Bytes(), err
}

// 获取 Go Package 源码目录
func PkgDir(pkgname string) string {
	return os.Getenv("GOPATH") + "/src/" + pkgname
}

// 获取程序所在目录
func AppDir() string {
	return filepath.Dir(os.Args[0])
}

func TimeToUnixString(srctime time.Time) string {
	return strconv.Itoa(int(srctime.Unix()))
}

func TimeToHttpString(srctime time.Time) string {
	return srctime.Format(time.RFC1123)
}


// []byte、string、map[string]string 的 interface{} 转换成 []byte
func ToBin(data interface{}) []byte {
	switch d := data.(type){
		case []byte:
			return d
		case string:
			return []byte(d)
		case map[string]string:
			b, _ := json.Marshal(d)
        	return b
	}
	return []byte{}
}

// []byte 转换成 map[string]string
func ToMap(b []byte) map[string]string {
	var m map[string]string
	json.Unmarshal(b, &m)
	return m
}
/*
// 获取扩展名
func Ext(path string) string {
	w := len(path)
	for i := w - 1; i >= 0 && path[i] != '/'; i-- {
		switch path[i] {
			case '.':
				return path[i:w]
			case '?':
				w = i
		}
	}
	return ""
}

// 清除 URL 参数
func UrlClean(path string) string {
	w := len(path) 

	for i := w - 1; i >= 0; i-- {
		switch path[i] {
			case '?':
				w = i
		}
	}
	
	return path[:w]
}

// BKDRHash算法
func Hash(data []byte) int {
	var h int
	for i := 0; i < len(data); i++ {
		h = h * 31 + int(data[i])
	}
	return h
}*/

// 虽然是弱Hash算法，但Http验证中，只要与上次不同就行，效率也更高
func Hash2(data []byte) int {
	var h int
	for i := 0; i < len(data); i++ {
		h += int(data[i])
	}
	return h
}