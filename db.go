package bamboo

import (
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/iterator"
	//"github.com/ssdb/gossdb/ssdb"
)

var db *leveldb.DB

// 打开数据库
func DbOpen() {
	db, _ = leveldb.OpenFile(rootDir +"/db", nil)
}

// 添加数据
func DbPut(key interface{}, value interface{}) error {
	return db.Put(ToBin(key), ToBin(value), nil)
}

// 取出数据
func DbGet(key interface{}) ([]byte, error) {
	return db.Get(ToBin(key), nil)
}

// 删除数据
func DbDel(key interface{}) error {
	return db.Delete(ToBin(key), nil)
}

// 创建迭代器对象，需要调用 .Release() 来释放
func DbIterator() iterator.Iterator {
	return db.NewIterator(nil)
}

// 关闭数据库
func DbClose() {
	defer db.Close()
}