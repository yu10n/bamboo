package bamboo

import (
	"net/http"
	"time"
	//"fmt"
)

var rootDir string
var pubDir string
var boDir string
var etagHat string
var srvBoValue map[string]string
var mimes map[string]string
var get map[string]func(*Http) = make(map[string]func(*Http))
var post map[string]func(*Http) = make(map[string]func(*Http))

// 初始化服务器
func init(){
	rootDir = AppDir()
	pubDir = "/public"
	boDir = "/bo"
	etagHat = TimeToUnixString(time.Now()) + "."
	
	initMdParse()
	srvBoValue = map[string]string{
		"BAMBOO.V": "Bamboo/1.1",
		"BAMBOO.DB": "GoLevelDB",
		"BAMBOO.LINK": "https://github.com/yulon/bamboo",
		"HTML.JS.JQ": QuoteJS("http://cdn.staticfile.org/jquery/2.1.1-rc2/jquery.min.js"),
		"HTML.CSS.BS.GRID": QuoteCSS("http://yulon.u.qiniudn.com/css/bootstrap.grid.css"),
		"HTML.CSS.BS": QuoteCSS("http://cdn.bootcss.com/bootstrap/3.2.0/css/bootstrap.min.css"),
		"HTML.CSS.BST": QuoteCSS("http://cdn.bootcss.com/bootstrap/3.2.0/css/bootstrap-theme.min.css"),
	}

	mimes = map[string]string{
		".html": "text/html", ".htm": "text/html",
		".bo": "text/html", ".md": "text/html",
		".xml": "text/xml", ".css": "text/css",
		".js": "text/javascript", ".txt": "text/plain",
		".png": "image/png", ".jpg": "image/jpeg", ".jfif": "image/jpeg",
		".jpeg": "image/jpeg", ".bmp": "image/bmp", ".ico": "image/x-icon",
		".gif": "image/gif", ".tiff": "image/tiff", ".tif": "image/tiff",
		".mp3": "video/mp3", ".mp4": "video/mpeg4", ".wav": "audio/wav",
	}
}

// 启动服务器
func Boom(addr string) {
	server := http.Server{
		Addr: addr,
		Handler: &srvDigest{},
	}
	server.ListenAndServe()
}

// 以TLS方式启动服务器
func BoomTLS() {
	server := http.Server{
		Addr: "443", //如果是被反向代理的其他端口，直接让代理服务器开启TLS功能就行
		Handler: &srvDigest{},
	}
	tlsdir := rootDir +"/tls/"
	server.ListenAndServeTLS(tlsdir + "ssl.crt", tlsdir + "ssl.key")
}

type srvDigest struct{}

func (*srvDigest) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	/*print("Scheme " + r.URL.Scheme + "\n")
	print("Opaque " + r.URL.Opaque + "\n")
	print("Host " + r.URL.Host + "\n")
	print("Path " + r.URL.Path + "\n")
	print("RawQuery " + r.URL.RawQuery + "\n")
	print("Fragment " + r.URL.Fragment + "\n")*/

	h := Http{O: w, I: r, File: r.URL.Path, BoValue: map[string]string{}, Age: 300, sc: 200}

	w.Header().Set("Server", srvBoValue["BAMBOO.V"])

	var d int

	forloop:
	for i := len(h.File) - 1; i >= 0; i-- {

		switch h.File[i]{
			case '.':
				if d == 0{
					d = i
					h.Ext = h.File[d:]
				}
			case '/':
				
				if d == 0{
					h.URI = h.File[i+1:]
				}else{
					h.URI = h.File[i+1:d]
				}
				h.Dir = h.File[:i+1]

				break forloop
		}

	}

	var f func(*Http)
	var ok bool

	switch r.Method{
		case "GET":
			f, ok = get[h.Dir+h.Ext]
			if ok == false{
				f, ok = get[h.Ext]
				if ok == false{
					f, ok = get[h.Dir]
				}
			}
		case "POST":
			f, ok = post[h.Dir+h.Ext]
			if ok == false{
				f, ok = post[h.Ext]
				if ok == false{
					f, ok = post[h.Dir]
				}
			}
	}

	if ok{
		f(&h)
	}

	if h.ret == false{
		if h.sc == 200{
			h.RetDefault()
		}else{
			h.Print()
		}
	}
}

func Get(route string, handler func(*Http)){
	get[route] = handler
}

func Post(route string, handler func(*Http)){
	post[route] = handler
}